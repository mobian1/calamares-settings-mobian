# Commented out values are defaults.
# All commands are running with 'sh -c'.
---
#######
### Target OS information
#######

## Operating System Name
osName: "Mobian"

## User Interface name (e.g. Plasma Mobile)
userInterface: "Phosh"

## OS version
version: "Bookworm"

## Default username (for which the password will be set)
username: "mobian"

#######
### Target device information
#######

## Architecture (e.g. aarch64)
arch: "arm64"

## Name of the device (e.g. PinePhone)
device: "PinePhone"

## Partition that will be formatted and mounted (optionally with FDE) for the rootfs
# targetDeviceRoot: "/dev/unknown"

######
### Installer Features
######

## Ask whether sshd should be enabled or not. If enabled, add a dedicated ssh
## user with proper username and password and suggest to change to key-based
## authentication after installation.
featureSshd: false
#
## Ask the user, which filesystem to use.
featureFsType: true
## Filesystems that the user can choose from.
fsModel:
  - ext4
  - btrfs
  #- f2fs
## Default filesystem to display in the dialog. If featureFsType is disabled,
## this gets used without asking the user.
defaultFs: ext4

#######
### Commands running in the installer OS
#######

## Format the target partition with LUKS
## Arguments: <device>
## Stdin: password with \n
# cmdLuksFormat: "cryptsetup luksFormat --use-random"

## Open the formatted partition
## Arguments: <device> <mapping name>
## Stdin: password with \n
# cmdLuksOpen: "cryptsetup luksOpen"

## Format the rootfs with a file system
## Arguments: <device>
cmdMkfsRootExt4: "mkfs.ext4 -F -L 'root'"
cmdMkfsRootF2fs: "mkfs.f2fs -f -l 'root'"
cmdMkfsRootBtrfs: "mkfs.btrfs-mobian"

## Mount the partition after formatting with file system
## Arguments: <device> <mountpoint>
# cmdMount: "mount"

#######
### Commands running in the target OS (chroot)
#######

## Set the password for default user and sshd user
## Arguments: <username>
## Stdin: password twice, each time with \n
# cmdPasswd: "passwd"

## Enable or disable sshd
# cmdSshdEnable: "systemctl enable sshd.service"
# cmdSshdDisable: "systemctl disable sshd.service"

## Create the user for sshd
## Arguments: <username>
# cmdSshdUseradd: "useradd -G wheel -m"

## Run to partition and format internal storage
cmdInternalStoragePrepare: prepare-internal-storage
